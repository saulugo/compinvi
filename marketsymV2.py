import pandas as pd
import numpy as np
import math
import copy
import QSTK.qstkutil.qsdateutil as du
import datetime as dt
import QSTK.qstkutil.DataAccess as da
import QSTK.qstkutil.tsutil as tsu
import QSTK.qstkstudy.EventProfiler as ep
import csv


def read_orders(filename):
    reader = csv.reader(open(filename,'rU'),delimiter=',')

    return reader

def read_dates_sym(reader):
    ls_dates = []
    ls_dates_un = []
    ls_sym = []
    ls_sym_un = []

    for row in reader:
        year = int(row[0])
        month = int(row[1])
        day = int(row[2])
        date = dt.datetime(year,month,day)
        ls_dates.append(date)
        ls_sym.append(row[3])

    ls_dates_un = list(set(ls_dates))
    ls_dates_un.sort()

    ls_sym_un = list(set(ls_sym))
    ls_sym_un.sort()

    return ls_dates_un, ls_sym_un

def read_price_data(ls_dates,ls_sym):
    dt_timeofday = dt.timedelta(hours=16)
    ls_dates_aux = []

    for i in range(0,len(ls_dates)):
        date = ls_dates[i] + dt_timeofday
        ls_dates_aux.append(date)

    c_dataobj = da.DataAccess('Yahoo')
    ls_keys = ['close']

    ldf_data = c_dataobj.get_data(ls_dates_aux,ls_sym,ls_keys)

    d_data = dict(zip(ls_keys,ldf_data))

    na_price = d_data['close'].values

    na_price_aux = np.array(na_price)
    
    return na_price_aux

def create_df_orders(ls_dates,ls_sym,reader_aux):
    df = pd.DataFrame(index=ls_dates,columns=ls_sym)

    df = df.fillna(0)

    for row in reader_aux:
        year = int(row[0])
        month = int(row[1])
        day = int(row[2])
        date = dt.datetime(year,month,day)
        sym = row[3]
        operation = row[4]
        qty = int(row[5])
        if operation=='Buy':
            df[sym][date] = df[sym][date] + qty
        if operation=='Sell':
            df[sym][date] = df[sym][date] - qty

    return df

def create_df_prices(ls_dates,ls_sym,na_price_aux):
    df = pd.DataFrame(na_price_aux,index=ls_dates,columns=ls_sym)
    df['Cash'] = 1.0
    return df

def create_df_holdings(ls_dates,ls_sym,df_prices,df_trades,cash):
    df_holdings = pd.DataFrame(0,index=ls_dates,columns=ls_sym)
    df_holdings['Cash'] = 0
    df_holdings['Cash_Used'] = 0
    df_holdings['Portfolio'] = 0

    dt_date_ini = ls_dates[0]-dt.timedelta(hours=24)
    dt_date_end = ls_dates[len(ls_dates)-1]+dt.timedelta(hours=24)


    line = pd.DataFrame({ls_sym[0]: 0}, index=[dt_date_ini])
    for i in range(1,len(ls_sym)):
        line[ls_sym[i]]=0
    
    line['Cash'] = cash
    line['Cash_Used'] = 0.0
    line['Portfolio'] = cash
    df2 = pd.concat([line,df_holdings])

    line2 = pd.DataFrame({ls_sym[0]: 0}, index=[dt_date_end])
    for i in range(1,len(ls_sym)):
        line2[ls_sym[i]]=0
    
    line2['Cash'] = 0.0
    line2['Cash_Used'] = 0.0
    line2['Portfolio'] = 0.0

    df3 = pd.concat([df2,line2])

    dfh = df3

    line3 = pd.DataFrame({ls_sym[0]: 0}, index=[dt_date_end])
    for i in range(1,len(ls_sym)):
        line3[ls_sym[i]]=0
    
    dft = pd.concat([df_trades,line3])

    line4 = pd.DataFrame({ls_sym[0]: 0}, index=[dt_date_end])
    for i in range(1,len(ls_sym)):
        line4[ls_sym[i]]=0
    
    line4['Cash'] = 1

    dfp = pd.concat([df_prices,line4])
 
    for j in range(0,len(ls_sym)):
        dfp[ls_sym[j]][len(ls_dates)] = dfp[ls_sym[j]][len(ls_dates)-1]

    print dfp
    print dft

    for i in range(1,len(dfh[ls_sym[0]][:])):
        k = i-1
        for j in range(0,len(ls_sym)):
            dfh[ls_sym[j]][i] = dfh[ls_sym[j]][i-1] + dft[ls_sym[j]][k]
        cashu = 0.0
        for j in range(0,len(ls_sym)):
            cashu = cashu + dft[ls_sym[j]][k]*dfp[ls_sym[j]][k]
        dfh['Cash_Used'][i] = cashu
        dfh['Cash'][i] = dfh['Cash'][i-1] - dfh['Cash_Used'][i]
        stocks = 0.0
        for j in range(0,len(ls_sym)):
            stocks = stocks + dfh[ls_sym[j]][i]*dfp[ls_sym[j]][k]
        dfh['Portfolio'][i] = dfh['Cash'][i] + stocks

    return dfh




                                      

            


        
#############################################################################
if __name__ == '__main__':
    reader = read_orders('orders.csv')
    reader_aux = read_orders('orders.csv')
    cash = 1000000

    ls_dates, ls_sym = read_dates_sym(reader)
    
    na_price = read_price_data(ls_dates,ls_sym)

    df_trades = create_df_orders(ls_dates,ls_sym,reader_aux)
    df_prices = create_df_prices(ls_dates,ls_sym,na_price)
    df_holdings = create_df_holdings(ls_dates,ls_sym,df_prices,df_trades,cash)

#    print df_prices
#    print df_trades
    print df_holdings




    
