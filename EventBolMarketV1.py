'''
(c) 2011, 2012 Georgia Tech Research Corporation
This source code is released under the New BSD license.  Please see
http://wiki.quantsoftware.org/index.php?title=QSTK_License
for license details.

Created on January, 23, 2013

@author: Sourabh Bajaj
@contact: sourabhbajaj@gatech.edu
@summary: Event Profiler Tutorial

Saul Lugo: I am modifiying this code in order to complete HW2.
'''


import pandas as pd
import numpy as np
import math
import copy
import QSTK.qstkutil.qsdateutil as du
import datetime as dt
import QSTK.qstkutil.DataAccess as da
import QSTK.qstkutil.tsutil as tsu
import QSTK.qstkstudy.EventProfiler as ep
import csv as csv

"""
Accepts a list of symbols along with start and end date
Returns the Event Matrix which is a pandas Datamatrix
Event matrix has the following structure :
    |IBM |GOOG|XOM |MSFT| GS | JP |
(d1)|nan |nan | 1  |nan |nan | 1  |
(d2)|nan | 1  |nan |nan |nan |nan |
(d3)| 1  |nan | 1  |nan | 1  |nan |
(d4)|nan |  1 |nan | 1  |nan |nan |
...................................
...................................
Also, d1 = start date
nan = no information about any event.
1 = status bit(positively confirms the event occurence)
"""


def find_events(ls_symbols, d_data,lookback):
    ''' Finding the event dataframe '''
    df_close = d_data['close']

    filename = 'bollinger.csv'
    writer = csv.writer(open(filename,'wb'),delimiter=',')
    print "Computing Bollinger Values"

    # Creating an empty dataframe
    df_events = copy.deepcopy(df_close)
    df_events = df_events * np.NAN
    df_bollinger = copy.deepcopy(df_events)
    df_bollinger = df_bollinger * np.NAN

    # Time stamps for the event range
    ldt_timestamps = df_close.index
    event_count = 0
    
    for s_sym in ls_symbols:
        ls_price = []
        for i in range(0, len(ldt_timestamps)):
            price = df_close[s_sym].ix[ldt_timestamps[i]]
            if i<lookback:
                ls_price.append(price)

            if i==lookback-1:
                stdev = np.std(ls_price)
                mean = np.average(ls_price)
                value = (price - mean)/stdev
                df_bollinger[s_sym].ix[ldt_timestamps[i]] = value
                
            if i>=lookback:
                ls_price.append(price)
                ls_price.pop(0)
                stdev = np.std(ls_price)
                mean = np.average(ls_price)
                value = (price - mean)/stdev
                df_bollinger[s_sym].ix[ldt_timestamps[i]] = value


    
    print 'Finding Events'
    for s_sym in ls_symbols:
        for i in range(lookback,len(ldt_timestamps)):
            bol_yesterday = df_bollinger[s_sym].ix[ldt_timestamps[i-1]]
            bol_today = df_bollinger[s_sym].ix[ldt_timestamps[i]]
            bol_spy = df_bollinger['SPY'].ix[ldt_timestamps[i]]
            if bol_yesterday>=-2 and bol_today<=-2 and bol_spy>=1:
                event_count += 1
                #print 'Event number: ',event_count
                #print 'Bollinger yesterday-today-spy ', bol_yesterday, bol_today, bol_spy
                df_events[s_sym].ix[ldt_timestamps[i]] = 1
                
    print 'Events found: ',event_count
          
    print 'Writting Bollinger values to a file'

    row = []
    row.append('Dates')
    for s_sym in ls_symbols:
        row.append(s_sym)

    writer.writerow(row)

    for i in range(0,len(ldt_timestamps)):
        row = []
        row.append(ldt_timestamps[i])
        for s_sym in ls_symbols:
            row.append(df_bollinger[s_sym].ix[ldt_timestamps[i]])
        
        writer.writerow(row)
            
    print 'File ready!'

    return df_events



if __name__ == '__main__':
    dt_start = dt.datetime(2008, 1, 1)
    dt_end = dt.datetime(2009, 12, 31)
    lookback = 20
    ldt_timestamps = du.getNYSEdays(dt_start, dt_end, dt.timedelta(hours=16))

    print "Reading market data, please wait!"

    dataobj = da.DataAccess('Yahoo')
    ls_symbols = dataobj.get_symbols_from_list('sp5002012')
    ls_symbols.append('SPY')

    ls_keys = ['close']
    ldf_data = dataobj.get_data(ldt_timestamps, ls_symbols, ls_keys)
    d_data = dict(zip(ls_keys, ldf_data))

    for s_key in ls_keys:
        d_data[s_key] = d_data[s_key].fillna(method='ffill')
        d_data[s_key] = d_data[s_key].fillna(method='bfill')
        d_data[s_key] = d_data[s_key].fillna(1.0)

    df_events = find_events(ls_symbols, d_data,lookback)

    print "Creating Study"
    
    ep.eventprofiler(df_events, d_data, i_lookback=20, i_lookforward=20,
                s_filename='MyEventBollinger.pdf', b_market_neutral=True, b_errorbars=True,
                s_market_sym='SPY')

    print 'Done!'
    #print df_events.ix[200:220,:8]


    
