#Author: Saul Lugo
#Contact: saullugolugo@gmail.com
#Content: this code was written as part of Homework1
# of computational investing I course in coursera.
#Date: Sep 15th 2013.
###################################################

#Function: simulate(dt_start,dt_end,ar_symbols,ar_allocations)
#Return: stdev = standard deviation of daily returns.
#avret = average daily return of total portfolio.
#shratio = Sharpe Ratio of total portfolio, 
#assuming 252 trading dates in a year and 
#risk free rate of 0.
#cumret = cummulative return to total portafolio.
#Example of function call:
#stdev, avret, shratio, cumret = simulate(dt_start,dtend,["GOOG", "AAPL", "GLD", "XOM"],[0.2,0.3,0.4,0.1]

#Assumptions: allocate some amount to the portfolio
#on the start date and then hold it through the 
#entire period. The prices at the end of the date
#are adjusted close data.
###################################################

#Import QSTK libraries
import QSTK.qstkutil.qsdateutil as du
import QSTK.qstkutil.tsutil as tsu
import QSTK.qstkutil.DataAccess as da

#Import others libraries
import datetime as dt
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np



def simulate(dt_start,dt_end,ar_symbols,ar_allocations):
    #The prices are taken at 4pm EST, so the following timestamp has a delta of 16 hours.
    dt_timeofday = dt.timedelta(hours=16)

    #Get a list of trading days between the start and end date.
    ldt_timestamps = du.getNYSEdays(dt_start,dt_end,dt_timeofday)

    #Creating an object of the dataaccess class with Yahoo as source.
    c_dataobj = da.DataAccess('Yahoo')

    #Keys to be read from the data. In this case I only care for 'close'
    #An example of ls_keys for other set of data is ls_keys = ['open', 'high', 'low', 'close', 'volume', 'actual_close']
    ls_keys = ['close']

    #Reading the data using the data obj c_dataobj
    ldf_data = c_dataobj.get_data(ldt_timestamps, ar_symbols, ls_keys)

    #d_data is a dictionary with the keys in ls_keys
    d_data = dict(zip(ls_keys, ldf_data))

    # Getting the numpy ndarray of close prices.
    na_price = d_data['close'].values

    
    na_price_aux = np.array(na_price)
    na_rets = tsu.returnize0(na_price)

    np_na_rets = np.array(na_rets)
    daily_ret_matrix = np.empty(na_price_aux.shape)

    cum_ret = np.empty(na_price_aux.shape[1])
    cum_ret_aux = np.empty(na_price_aux.shape[1])
    port_dret = np.empty(na_price_aux.shape[0])

    #retPosition = FinalValue/InicialValue - 1

    rows = na_price_aux.shape[0]-1
    

    for i in range(len(cum_ret)):
        cum_ret[i] = na_price_aux[rows,i]/na_price_aux[0,i] - 1
        cum_ret_aux[i] = ar_allocations[i]*(1 + cum_ret[i])

    ret_port = 0.0
    for i in range(len(cum_ret_aux)):
        ret_port = ret_port + cum_ret_aux[i]

    #this loop fill out the matrix of daily returns multiply by the allocations
    for j in range(na_price_aux.shape[1]):
        for i in range(na_price_aux.shape[0]):
            daily_ret_matrix[i,j]=ar_allocations[j]*np_na_rets[i,j]

        
    for i in range(len(port_dret)):
        port_dret[i] = np.sum(daily_ret_matrix[i,:])
        
    stdev = np.std(port_dret)
    avedailyr = np.average(port_dret)
    k = 252 ** 0.5
    sharperatio = k * avedailyr/stdev

    return stdev, avedailyr, sharperatio, ret_port
    
#Example of function call:
#stdev, avret, shratio, cumret = simulate(dt_start,dtend,["GOOG", "AAPL", "GLD", "XOM"],[0.2,0.3,0.4,0.1

if __name__ == '__main__':
    dt_start = dt.datetime(2010, 1, 1)
    dt_end = dt.datetime(2010,12,31)
    ls_symbols = ["AXP","HPQ","IBM","HNZ"]
    ar_allocations = [0.0,0.0,0.0,0.1]

    #ar_solutions = [stdev, avedailyr, sharperatio, ret_port]
    ar_solutions = simulate(dt_start,dt_end,ls_symbols,ar_allocations)

    print "Start Date: ", dt_start
    print "End Date: ", dt_end
    print "Symbols :", ls_symbols
    print "Optimal Allocations: ", ar_allocations
    print "Sharpe Ratio: ", ar_solutions[2]
    print "Volatility (Stdev of daily returns): ", ar_solutions[0]
    print "Average daily return: ", ar_solutions[1]
    print "Cummulative portfolio return: ", ar_solutions[3]
