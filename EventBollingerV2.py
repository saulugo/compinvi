'''
(c) 2011, 2012 Georgia Tech Research Corporation
This source code is released under the New BSD license.  Please see
http://wiki.quantsoftware.org/index.php?title=QSTK_License
for license details.

Created on January, 23, 2013

@author: Sourabh Bajaj
@contact: sourabhbajaj@gatech.edu
@summary: Event Profiler Tutorial

Saul Lugo: I am modifiying this code in order to complete HW2.
'''

import sys
import csv
import pandas as pd
import numpy as np
import math
import copy
import QSTK.qstkutil.qsdateutil as du
import datetime as dt
import QSTK.qstkutil.DataAccess as da
import QSTK.qstkutil.tsutil as tsu
import QSTK.qstkstudy.EventProfiler as ep
import matplotlib.pyplot as plt

"""
Accepts a list of symbols along with start and end date
Returns the Event Matrix which is a pandas Datamatrix
Event matrix has the following structure :
    |IBM |GOOG|XOM |MSFT| GS | JP |
(d1)|nan |nan | 1  |nan |nan | 1  |
(d2)|nan | 1  |nan |nan |nan |nan |
(d3)| 1  |nan | 1  |nan | 1  |nan |
(d4)|nan |  1 |nan | 1  |nan |nan |
...................................
...................................
Also, d1 = start date
nan = no information about any event.
1 = status bit(positively confirms the event occurence)
"""

def find_sell_date(dt_date):
    dt_aux = dt.timedelta(days=30)
    dt_end = dt_date + dt_aux
    dt_dateaux = du.getNYSEdays(dt_date, dt_end, dt.timedelta(hours=16))

    return dt_dateaux[5]

def calculate_bollinger(ls_symbols, d_data,lookback):

    df_close = d_data['close']


    print "Calculating Bollinger values"

    df_bollinger = copy.deepcopy(df_close)
    df_bollinger = df_bollinger * np.NAN

    # Time stamps for the event range
    ldt_timestamps = df_close.index



    
    for s_sym in ls_symbols:
        ls_price = []
        for i in range(0, len(ldt_timestamps)):
            price  = df_close[s_sym].ix[ldt_timestamps[i]]
            if i<lookback:
               ls_price.append(price)

            if i==lookback-1:
                stdev = np.std(ls_price)
                mean = np.average(ls_price)
                value = (price - mean)/stdev
                df_bollinger[s_sym].ix[ldt_timestamps[i]] = value
                
            if i>=lookback:
                ls_price.append(price)
                ls_price.pop(0)
                stdev = np.std(ls_price)
                mean = np.average(ls_price)
                value = (price - mean)/stdev
                df_bollinger[s_sym].ix[ldt_timestamps[i]] = value

    return df_bollinger
        
def find_sell_date(ldt_timestamps,i):
    dt_aux = dt.timedelta(days=30)
    dt_end = ldt_timestamps[i] + dt_aux

    dt_dateaux = du.getNYSEdays(ldt_timestamps[i],dt_end,dt.timedelta(hours=16))

    return dt_dateaux[5]
    
def count_events(df_bollinger,ls_symbols,ldt_timestamps,lookback):
    
    df_events = copy.deepcopy(df_bollinger)
    df_events = df_events * np.NAN
    writer = csv.writer(open('orders.csv','wb'),delimiter=',')


    count = 0
    for sym in ls_symbols:
        for i in range(lookback,len(ldt_timestamps)):
            val_today = df_bollinger[sym].ix[ldt_timestamps[i]]
            val_yesterday = df_bollinger[sym].ix[ldt_timestamps[i-1]]
            val_spy = df_bollinger['SPY'].ix[ldt_timestamps[i]]
            if val_today <= -2.03 and val_yesterday >= -1.97 and val_spy >= 1.53:
                row = []
                count = count + 1
                print 'Event: ',count
                print 'Yes-Tod-Spy',ldt_timestamps[i],sym,val_yesterday,val_today,val_spy
                df_events[sym].ix[ldt_timestamps[i]] = 1
                row.append(ldt_timestamps[i].year)
                row.append(ldt_timestamps[i].month)
                row.append(ldt_timestamps[i].day)
                row.append(sym)
                row.append('Buy')
                row.append(100)
                writer.writerow(row)

                date_aux = find_sell_date(ldt_timestamps,i)
                row = []
                row.append(date_aux.year)
                row.append(date_aux.month)
                row.append(date_aux.day)
                row.append(sym)
                row.append('Sell')
                row.append(100)
                writer.writerow(row)
                

    return df_events, count
    
        
def writeToFile(df,ls_symbols,ldt_timestamps,filename):

    writer = csv.writer(open(filename,'wb'),delimiter=',')

    print 'Writting to file'

    row = []

    row.append('date')

    for sym in ls_symbols:
        row.append(sym)

    writer.writerow(row)

    row=[]

    for i in range(0,len(ldt_timestamps)):
        row.append(ldt_timestamps[i])
        for sym in ls_symbols:
            row.append(df[sym][ldt_timestamps[i]])
        writer.writerow(row)
        row = []
    print 'Write to file done!'




if __name__ == '__main__':
    dt_start = dt.datetime(2008, 1, 1)
    dt_end = dt.datetime(2009, 12, 31)
    lookback = 20
    ldt_timestamps = du.getNYSEdays(dt_start, dt_end, dt.timedelta(hours=16))
    filename = 'bolout.csv'
    
    dataobj = da.DataAccess('Yahoo')
    ls_symbols = dataobj.get_symbols_from_list('sp5002012')
    ls_symbols.append('SPY')



    #ls_keys = ['open', 'high', 'low', 'close', 'volume', 'actual_close']
    ls_keys = ['close']

    print "Reading price data"
    ldf_data = dataobj.get_data(ldt_timestamps, ls_symbols, ls_keys)
    d_data = dict(zip(ls_keys, ldf_data))

    print "Preparing the data"
    for s_key in ls_keys:
        d_data[s_key] = d_data[s_key].fillna(method='ffill')
        d_data[s_key] = d_data[s_key].fillna(method='bfill')
        d_data[s_key] = d_data[s_key].fillna(1.0)

    
    df_bollinger = calculate_bollinger(ls_symbols, d_data,lookback)
    
    writeToFile(df_bollinger,ls_symbols,ldt_timestamps,filename)

    df_events, event_count = count_events(df_bollinger,ls_symbols,ldt_timestamps,lookback)

    print 'Event Count: ',event_count
    
    print "Creating Study"
    
    ep.eventprofiler(df_events, d_data, i_lookback=20, i_lookforward=20,
                s_filename='MyEventBollinger.pdf', b_market_neutral=True, b_errorbars=True,
                s_market_sym='SPY')


