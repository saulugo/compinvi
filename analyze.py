import sys
import pandas as pd
import numpy as np
import math
import copy
import QSTK.qstkutil.qsdateutil as du
import datetime as dt
import QSTK.qstkutil.DataAccess as da
import QSTK.qstkutil.tsutil as tsu
import QSTK.qstkstudy.EventProfiler as ep
import csv


def read_portfolio(filename):
    lsd = []
    lsv = []
    reader = csv.reader(open(filename,'rU'),delimiter=',')

    for row in reader:
        year = int(row[0])
        month = int(row[1])
        day = int(row[2])
        date = dt.datetime(year,month,day)
        lsd.append(date)
        lsv.append(float(row[3]))

    lsc =  ['Portfolio','Portfolio_na','Daily_Ret']
    df = pd.DataFrame(index=lsd,columns=lsc)
    dts = lsd[1]
    dte = lsd[len(lsd)-2]

    for i in range(0,len(lsd)):
        df['Portfolio'][i]=lsv[i]
        df['Portfolio_na'][i]=lsv[i]/lsv[0]
        if i==0:
            df['Daily_Ret'][i]=0.0
        else:
            df['Daily_Ret'][i]=df['Portfolio'][i]/df['Portfolio'][i-1]-1
        
    daily_ret = []

    for i in range(1,len(lsd)-1):
        daily_ret.append(df['Daily_Ret'][i])

        

    stdev = np.std(daily_ret)

    averet = np.average(daily_ret)

    k = 252 ** 0.5

    sharpe = k * (averet/stdev)

    print 'FUND: '
    print 'Stdev: ',stdev
    print 'Average Return: ',averet
    print 'Sharpe: ',sharpe
    print 'Total Return: ',df['Portfolio_na'][len(lsv)-1]

    return df, dts, dte


def read_price_data(dt_start,dt_end,bm):

    #The prices are taken at 4pm EST, so the following timestamp has a delta of 16 hours.
    dt_timeofday = dt.timedelta(hours=16)

    dt_timeend = dt.timedelta(hours=24)

    dte = dt_end + dt_timeend

    #Get a list of trading days between the start and end date.
    ldt_timestamps = du.getNYSEdays(dt_start,dte,dt_timeofday)

    #Creating an object of the dataaccess class with Yahoo as source.
    c_dataobj = da.DataAccess('Yahoo')

    #Keys to be read from the data. In this case I only care for 'close'
    #An example of ls_keys for other set of data is ls_keys = ['open', 'high', 'low', 'close', 'volume', 'actual_close']
    ls_keys = ['close']

    ar_symbols = []

    ar_symbols.append(bm)

    #Reading the data using the data obj c_dataobj
    ldf_data = c_dataobj.get_data(ldt_timestamps, ar_symbols, ls_keys)

    #d_data is a dictionary with the keys in ls_keys
    d_data = dict(zip(ls_keys, ldf_data))

    # Getting the numpy ndarray of close prices.
    na_price = d_data['close'].values

    lsc = ['SP500','SP500_NA','DR']
    df = pd.DataFrame(index=range(0,len(na_price)),columns=lsc)

    for i in range(0,len(na_price)):
        df['SP500'][i] = na_price[i]
        df['SP500_NA'][i] = df['SP500'][i]/df['SP500'][0]
        if i==0:
            df['DR'][i]=0
        else:
            df['DR'][i]=df['SP500'][i]/df['SP500'][i-1]-1

   # print df[:][0:50]
    
    daily_ret = []

    for i in range(0,len(na_price)):
        daily_ret.append(df['DR'][i])
        
    totret = df['SP500_NA'][len(na_price)-1]
    std5 = np.std(daily_ret)
    ave = np.average(daily_ret)
    k = 252**0.5
    sharpe = k*ave/std5
    print 'S&P500'
    print 'STD: ',std5
    print 'AVE: ',ave
    print 'Shape: ',sharpe
    print 'Total Return: ', totret

    print 'Date Start: ',dt_start
    print 'Date End: ',dt_end



    
        
#############################################################################
# python analyze.py portfolio.csv \$SPX
if __name__ == '__main__':
    dfp, dts, dte = read_portfolio(sys.argv[1])
    bm = sys.argv[2]
    

    
    dfpb = read_price_data(dts,dte,bm)

    










    
