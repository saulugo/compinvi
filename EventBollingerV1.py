'''
(c) 2011, 2012 Georgia Tech Research Corporation
This source code is released under the New BSD license.  Please see
http://wiki.quantsoftware.org/index.php?title=QSTK_License
for license details.

Created on January, 23, 2013

@author: Sourabh Bajaj
@contact: sourabhbajaj@gatech.edu
@summary: Event Profiler Tutorial

Saul Lugo: I am modifiying this code in order to complete HW2.
'''

import sys
import csv
import pandas as pd
import numpy as np
import math
import copy
import QSTK.qstkutil.qsdateutil as du
import datetime as dt
import QSTK.qstkutil.DataAccess as da
import QSTK.qstkutil.tsutil as tsu
import QSTK.qstkstudy.EventProfiler as ep
import matplotlib.pyplot as plt

"""
Accepts a list of symbols along with start and end date
Returns the Event Matrix which is a pandas Datamatrix
Event matrix has the following structure :
    |IBM |GOOG|XOM |MSFT| GS | JP |
(d1)|nan |nan | 1  |nan |nan | 1  |
(d2)|nan | 1  |nan |nan |nan |nan |
(d3)| 1  |nan | 1  |nan | 1  |nan |
(d4)|nan |  1 |nan | 1  |nan |nan |
...................................
...................................
Also, d1 = start date
nan = no information about any event.
1 = status bit(positively confirms the event occurence)
"""

def find_sell_date(dt_date):
    dt_aux = dt.timedelta(days=30)
    dt_end = dt_date + dt_aux
    dt_dateaux = du.getNYSEdays(dt_date, dt_end, dt.timedelta(hours=16))

    return dt_dateaux[5]

def calculate_bollinger(ls_symbols, d_data,lookback, filename):

    df_close = d_data['close']

    writer = csv.writer(open(filename,'wb'),delimiter=',')

    print "Calculating Bollinger values"

    df_bollinger = copy.deepcopy(df_close)
    df_bollinger = df_bollinger * np.NAN

    df_plotter = copy.deepcopy(df_bollinger)

    df_plotter['Mean'] = np.NAN
    df_plotter['H_Bollinger'] = np.NAN
    df_plotter['L_Bollinger'] = np.NAN

    # Time stamps for the event range
    ldt_timestamps = df_close.index

    #Plotting the price

    plt.clf()
    plt.plot(ldt_timestamps,df_close)
    plt.legend(ls_symbols)
    plt.ylabel('Adjusted Close')
    plt.xlabel('Date')
    plt.savefig('adjustedclose.pdf',format='pdf')
    print "Plotting price done!"

    ls_price = []

    
    for s_sym in ls_symbols:
        for i in range(0, len(ldt_timestamps)):
            price  = df_close[s_sym].ix[ldt_timestamps[i]]
            if i<lookback:
               ls_price.append(price)

            if i==lookback-1:
                stdev = np.std(ls_price)
                mean = np.average(ls_price)
                value = (price - mean)/stdev
                h_b = mean + stdev
                l_b = mean - stdev
                df_bollinger[s_sym].ix[ldt_timestamps[i]] = value
                df_plotter[s_sym].ix[ldt_timestamps[i]] = price
                df_plotter['Mean'].ix[ldt_timestamps[i]] = mean
                df_plotter['H_Bollinger'].ix[ldt_timestamps[i]] = h_b
                df_plotter['L_Bollinger'].ix[ldt_timestamps[i]] = l_b
                
            if i>=lookback:
                ls_price.append(price)
                ls_price.pop(0)
                stdev = np.std(ls_price)
                mean = np.average(ls_price)
                value = (price - mean)/stdev
                df_bollinger[s_sym].ix[ldt_timestamps[i]] = value
                h_b = mean + stdev
                l_b = mean - stdev
                df_bollinger[s_sym].ix[ldt_timestamps[i]] = value
                df_plotter[s_sym].ix[ldt_timestamps[i]] = price
                df_plotter['Mean'].ix[ldt_timestamps[i]] = mean
                df_plotter['H_Bollinger'].ix[ldt_timestamps[i]] = h_b
                df_plotter['L_Bollinger'].ix[ldt_timestamps[i]] = l_b


    print "Writing Bollinger values to the file ",filename



    row = []
    row.append("Date")

    for s_sym in ls_symbols:
        row.append(s_sym)

    writer.writerow(row)
    
    for i in range(0,len(ldt_timestamps)):
        row = []
        row.append(ldt_timestamps[i])
        for s_sym in ls_symbols:
            row.append(df_bollinger[s_sym].ix[ldt_timestamps[i]])
        writer.writerow(row)

    #Plotting the Bollinger Bands

    plt.clf()
    plt.plot(ldt_timestamps,df_plotter)
    plt.legend(ls_symbols)
    plt.ylabel('Bollinger Bands')
    plt.xlabel('Date')
    plt.savefig('bollinger.pdf',format='pdf')
    print "Plotting Bollinger done!"



    print "Done!"
    
    
        


if __name__ == '__main__':
    dt_start = dt.datetime(2010, 1, 1)
    dt_end = dt.datetime(2010, 12, 31)
    filename = 'quiz5.csv'
    lookback = 20
    ldt_timestamps = du.getNYSEdays(dt_start, dt_end, dt.timedelta(hours=16))

    dataobj = da.DataAccess('Yahoo')
    ls_symbols = ['MSFT']


    ls_keys = ['open', 'high', 'low', 'close', 'volume', 'actual_close']

    print "Reading price data"
    ldf_data = dataobj.get_data(ldt_timestamps, ls_symbols, ls_keys)
    d_data = dict(zip(ls_keys, ldf_data))

    print "Preparing the data"
    for s_key in ls_keys:
        d_data[s_key] = d_data[s_key].fillna(method='ffill')
        d_data[s_key] = d_data[s_key].fillna(method='bfill')
        d_data[s_key] = d_data[s_key].fillna(1.0)

    
    calculate_bollinger(ls_symbols, d_data,lookback,filename)

