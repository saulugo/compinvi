#Author: Saul Lugo
#Contact: saullugolugo@gmail.com
#Content: this code was written as part of Homework1
# of computational investing I course in coursera.
#Date: Sep 15th 2013.
###################################################

#Function: simulate(dt_start,dt_end,ar_symbols,ar_allocations)
#Return: stdev = standard deviation of daily returns.
#avret = average daily return of total portfolio.
#shratio = Sharpe Ratio of total portfolio, 
#assuming 252 trading dates in a year and 
#risk free rate of 0.
#cumret = cummulative return to total portafolio.
#Example of function call:
#stdev, avret, shratio, cumret = simulate(dt_start,dtend,["GOOG", "AAPL", "GLD", "XOM"],[0.2,0.3,0.4,0.1]

#Assumptions: allocate some amount to the portfolio
#on the start date and then hold it through the 
#entire period. The prices at the end of the date
#are adjusted close data.
###################################################

#Import QSTK libraries
import QSTK.qstkutil.qsdateutil as du
import QSTK.qstkutil.tsutil as tsu
import QSTK.qstkutil.DataAccess as da

#Import others libraries
import datetime as dt
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np



def simulate(dt_start,dt_end,ar_symbols,ar_allocations):
    #The prices are taken at 4pm EST, so the following timestamp has a delta of 16 hours.
    dt_timeofday = dt.timedelta(hours=16)

    #Get a list of trading days between the start and end date.
    ldt_timestamps = du.getNYSEdays(dt_start,dt_end,dt_timeofday)

    #Creating an object of the dataaccess class with Yahoo as source.
    c_dataobj = da.DataAccess('Yahoo')

    #Keys to be read from the data. In this case I only care for 'close'
    #An example of ls_keys for other set of data is ls_keys = ['open', 'high', 'low', 'close', 'volume', 'actual_close']
    ls_keys = ['close']

    #Reading the data using the data obj c_dataobj
    ldf_data = c_dataobj.get_data(ldt_timestamps, ar_symbols, ls_keys)

    #d_data is a dictionary with the keys in ls_keys
    d_data = dict(zip(ls_keys, ldf_data))

    # Getting the numpy ndarray of close prices.
    na_price = d_data['close'].values

    
    na_price_aux = np.array(na_price)

    normalized_price = np.empty(na_price_aux.shape)

    daily_ret_matrix = np.empty(na_price_aux.shape)

    #port_ret column 0: daily value of total portfolio, column 1: daily returns of total portfolio
    port_ret = np.empty((na_price_aux.shape[0],2))

    #normalizing stocks prices dividing each date price by the fist date price
    for j in range(na_price_aux.shape[1]):
        for i in range(na_price_aux.shape[0]):
            normalized_price[i,j] = na_price_aux[i,j]/na_price_aux[0,j]

    #this loop fill out the matrix of daily returns multiply by the allocations
    for j in range(na_price_aux.shape[1]):
        for i in range(na_price_aux.shape[0]):
            daily_ret_matrix[i,j]=ar_allocations[j]*normalized_price[i,j]
    
    #calculating total portfolio value and total portfolio daily return

    for i in range(na_price_aux.shape[0]):
        if i==0:
            port_ret[i,0] = 1
            port_ret[i,1] = 0
        else:
            port_ret[i,0] = np.sum(daily_ret_matrix[i,:])
            port_ret[i,1] = port_ret[i,0]/port_ret[i-1,0]-1

    #stdev of total portfolio daily returns (volatility)
    stdev = np.std(port_ret[:,1])
    
    #average of total portfolio daily returns (expected return)
    averet = np.average(port_ret[:,1])

    #total portfolio return
    totret = port_ret[port_ret.shape[0]-1,0]

    K = 252 ** 0.5

    #portfolio Sharpe Ratio = K * averet/stdev
    sharpe = K * averet/stdev

    return stdev, averet, sharpe, totret

    
    
#Example of function call:
#stdev, avret, shratio, cumret = simulate(dt_start,dtend,["GOOG", "AAPL", "GLD", "XOM"],[0.2,0.3,0.4,0.1

def possible_allocations():
    allo_matrix = np.zeros((14641,5))
    col = allo_matrix.shape[1];
    row = allo_matrix.shape[0];

    i = 0

    
    for x1 in np.arange(0.0,1.1,0.1):
        for x2 in np.arange(0.0,1.1,0.1):
            for x3 in np.arange(0.0,1.1,0.1):
                for x4 in np.arange(0.0,1.1,0.1):
                    allo_matrix[i,0] = x4
                    allo_matrix[i,1] = x3
                    allo_matrix[i,2] = x2
                    allo_matrix[i,3] = x1
                    if np.sum(allo_matrix[i,0:4]) == 1:
                        allo_matrix[i,4] = 1
                    else:
                        allo_matrix[i,4] = 0
                    i = i+1

    possible_allo = np.sum(allo_matrix[:,4])
    
    arr_possible = np.empty((possible_allo,4))
            
    j = 0
    for i in range(row):
        if allo_matrix[i,4]==1.0:
            arr_possible[j,:] = allo_matrix[i,0:4]
            j = j+1

    return arr_possible
            

def best_allocation(dt_start,dt_end,ls_symbols,ar_allocations):
    #solutions[stdev, averet, sharpe, totret]
    solutions = np.empty(4)
    #shape_matrix store each possible allocations combination and the value of the share ratio of each portfolio
    sharpe_matrix = np.zeros((ar_allocations.shape[0],5))

    #sharpe_matrix[1,0:4] = ar_allocations[1,:]

    for i in range(ar_allocations.shape[0]):
        sharpe_matrix[i,0:4] = ar_allocations[i,:]
        solutions = simulate(dt_start,dt_end,ls_symbols,ar_allocations[i,:])
        sharpe_matrix[i,4] = solutions[2]


    order = sharpe_matrix[:,4].argsort()
    order_sharpe = np.take(sharpe_matrix,order,0)

    return order_sharpe[order_sharpe.shape[0]-1,0:4]
    

#Example of function call:
#stdev, avret, shratio, cumret = simulate(dt_start,dtend,["GOOG", "AAPL", "GLD", "XOM"],[0.2,0.3,0.4,0.1

def main():
    dt_start = dt.datetime(2011, 1, 1)
    dt_end = dt.datetime(2011,12,31)
    ls_symbols = ["BRCM","ADBE","AMD","ADI"]

    #solutions[stdev, averet, sharpe, totret]
    solutions = np.empty(4)
    
    ar_allocations = possible_allocations()
    best_allocations = best_allocation(dt_start,dt_end,ls_symbols,ar_allocations)

    solutions = simulate(dt_start,dt_end,ls_symbols,best_allocations)

    print "Start date: ", dt_start
    print "End date: ", dt_end
    print "Symbols: ", ls_symbols
    print "Optimal allocations: ", best_allocations
    print "Sharpe ratio: ", solutions[2]
    print "Volatility (stdev of daily returns): ", solutions[0]
    print "Average daily return: ", solutions[1]
    print "Portfolio cumulative return: ", solutions[3]
    




if __name__ == '__main__':
    main()

